var gulp = require('gulp');

var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var autoprefixer = require('gulp-autoprefixer');


var data = require('gulp-data');
var pug = require('gulp-pug');
var strip = require('gulp-strip-comments');
var path = require('path');
var fs = require('fs');


gulp.task('sassBuild', function(){
    gulp.src('./src/sass/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('fontsBuild', function(){
    return gulp.src('./src/fonts/**/*.*')
        .pipe(gulp.dest('./dist/fonts'))
});

gulp.task('pugBuild', function buildHTML(){
    return gulp.src('./src/pug/*.pug')
        .pipe(data( function(file) {
            return JSON.parse(fs.readFileSync('./src/pug/data.json'));
        } ))

        .pipe(
            pug({
                pretty: false
            })
        )
        .pipe(strip())
        .pipe(gulp.dest('./dist'));
});

var scriptsList = [
    './bower_components/jquery/dist/jquery.js',
    './src/scripts/script.js'
];


gulp.task('scriptsBuild', function(){
    gulp.src(scriptsList)
        .pipe(concat('build.js'))
        .pipe(gulp.dest('./src/scripts'))
});

gulp.task('uglifyScript', function(){
    gulp.src('./src/scripts/build.js')
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
});

gulp.task('imageBuild', function(){
    gulp.src(['./src/images/**.jpg', './src/images/**.png'])
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/images'))
});

gulp.task('build', ['sassBuild', 'fontsBuild', 'pugBuild', 'scriptsBuild', 'uglifyScript', 'imageBuild'], function(){

});