;(function(){
    $('.drop__trigger').click(function(e){
        e.preventDefault();
        var _ = $(this);

        if (!_.hasClass('_active')){
            _.addClass('_active');
            $('body').append("<div class='overlay'></div>");
            $('.overlay').click(function(){
                $(this).remove();
                _.removeClass('_active');
            })
        } else {
            _.removeClass('_active')
        }

        $('.drop__value').click(function(){
            var str = $(this).find('span').text();
            $(this).parents('.drop__inner').siblings().find('span').text(str);
            $(this).parents('.drop__inner').siblings().removeClass('_active');
            $('.overlay').remove();
        });
    });

}());